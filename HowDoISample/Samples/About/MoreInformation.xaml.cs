﻿using System;
using Microsoft.Maui.Controls.Xaml;
using Microsoft.Maui.Controls;
using Microsoft.Maui;

namespace ThinkGeo.UI.Maui.HowDoI
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MoreInformation : ContentPage
    {
        public MoreInformation()
        {
            InitializeComponent();
        }

        private async void Hyperlink_Tapped(object sender, EventArgs e)
        {
            await Launcher.OpenAsync(((TappedEventArgs)e).Parameter.ToString());
        }

        private async void HyperlinkButton_Tapped(object sender, EventArgs e)
        {
            await Launcher.OpenAsync(((ImageButton)sender).BindingContext.ToString());
        }
    }
}