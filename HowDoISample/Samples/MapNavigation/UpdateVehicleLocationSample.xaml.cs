﻿using System.Collections.ObjectModel;
using System.Reflection;
using ThinkGeo.Core;

namespace ThinkGeo.UI.Maui.HowDoI
{
    /// <summary>
    ///     This samples shows how to refresh points on the map based on some outside event
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpdateVehicleLocationSample
    {
        //private System.Timers.Timer _gpsTimer;
        private Collection<Vertex> _gpsPoints;
        private int _currentPointIndex;
        private InMemoryFeatureLayer _visitedRoutesLayer;
        private int _previousVertex;
        private Marker _vehicleMarker;
        private bool _initialized;

        public UpdateVehicleLocationSample()
        {
            InitializeComponent();
            Disappearing += (_, _) => MapView.OverlayDrawn -= MapView_OverlayDrawn;
        }

        private async void UpdateVehicleLocationSample_OnSizeChanged(object sender, EventArgs e)
        {
            if (_initialized)
                return;
            _initialized = true;

            // Add Cloud Maps as a background overlay
            var backgroundOverlay = new ThinkGeoCloudRasterMapsOverlay("9ap16imkD_V7fsvDW9I8r8ULxgAB50BX_BnafMEBcKg~", "vtVao9zAcOj00UlGcK7U-efLANfeJKzlPuDB9nw7Bp4K4UxU_PdRDg~~", ThinkGeoCloudRasterMapsMapType.Light_V2_X2);
            backgroundOverlay.TileCache = new FileRasterTileCache(FileSystem.Current.CacheDirectory, "ThinkGeoCloudRasterMaps");

            MapView.Overlays.Add("Background Maps", backgroundOverlay);

            MapView.IsRotationEnabled = true;

            var lineShape = await InitGpsData();

            // Create the marker of the vehicle
            _vehicleMarker = new Marker
            {
                PositionX = lineShape.Vertices[0].X,
                PositionY = lineShape.Vertices[0].Y,
                ImageSource = (StreamImageSource)ImageSource.FromResource("HowDoISample.Resources.Images.vehicle-location.png"),
                PixelWidth = 24,
                PixelHeight = 24
            };

            // create the layers for the routes.
            var routesLayer = new InMemoryFeatureLayer();
            routesLayer.InternalFeatures.Add(new Feature(lineShape));
            routesLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = LineStyle.CreateSimpleLineStyle(GeoColors.Yellow, 6, true);
            routesLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            _visitedRoutesLayer = new InMemoryFeatureLayer();
            _visitedRoutesLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = LineStyle.CreateSimpleLineStyle(GeoColors.Green, 6, true);
            _visitedRoutesLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            var layerOverlay = new LayerOverlay();
            layerOverlay.TileType = TileType.SingleTile;
            layerOverlay.Layers.Add(routesLayer);
            layerOverlay.Layers.Add(_visitedRoutesLayer);
            MapView.Overlays.Add(layerOverlay);

            MapView.OverlayDrawn += MapView_OverlayDrawn;

            // Create a marker overlay to show where the vehicle is
            var markerOverlay = new SimpleMarkerOverlay();
            // Add the marker to the overlay than add to the map. 
            markerOverlay.Markers.Add(_vehicleMarker);
            MapView.Overlays.Add(markerOverlay);

            MapView.CenterPoint = new PointShape(_gpsPoints[0]);
            MapView.MapScale = MapView.ZoomLevelSet.ZoomLevel18.Scale;
            await MapView.RefreshAsync();

            AerialBackgroundCheckBox.CheckedChanged += async (_, args) =>
            {
                backgroundOverlay.MapType = args.Value
                    ? ThinkGeoCloudRasterMapsMapType.Aerial_V2_X2
                    : ThinkGeoCloudRasterMapsMapType.Light_V2_X2;
                await MapView.RefreshAsync(new[] { backgroundOverlay });
            };
        }

        private async void MapView_OverlayDrawn(object sender, OverlaysDrawnMapViewEventArgs e)
        {
            await UpdateGpsPoint();
        }

        private async Task<LineShape> InitGpsData()
        {
            _gpsPoints = new();

            // read the csv file from the embed resource. 
            var assembly = Assembly.GetExecutingAssembly();
            await using var stream = assembly.GetManifestResourceStream("HowDoISample.Data.Csv.vehicle-route.csv");
            if (stream == null)
                return null;

            using var reader = new StreamReader(stream);

            // Convert GPS Points from Lat/Lon (srid:4326) to Spherical Mercator (Srid:3857), which is the projection of the base map
            var converter = new ProjectionConverter(4326, 3857);
            converter.Open();

            var lineShape = new LineShape();
            while (!reader.EndOfStream)
            {
                string location = await reader.ReadLineAsync();
                if (location == null)
                    continue;
                var posItems = location.Split(',');
                var lat = double.Parse(posItems[0]);
                var lon = double.Parse(posItems[1]);
                var vertexInSphericalMercator = converter.ConvertToExternalProjection(lon, lat);
                _gpsPoints.Add(vertexInSphericalMercator);
                lineShape.Vertices.Add(vertexInSphericalMercator);
            }
            converter.Close();

            _previousVertex = 0;
            return lineShape;
        }

        private async Task UpdateGpsPoint()
        {
            // the GpsTimerElapsed method was not raised in the UI thread and that's why we need DispatchAsync
            await Dispatcher.DispatchAsync(async () =>
            {
                if (_currentPointIndex >= _gpsPoints.Count)
                    return;

                var currentLocation = _gpsPoints[_currentPointIndex];
                var angle = GetRotationAngle(_currentPointIndex, _gpsPoints);

                UpdateVisitedRoutes(_currentPointIndex);

                // Update the markers position

                _vehicleMarker.UpdatePosition(currentLocation.X, currentLocation.Y);
                var animationSettings = new AnimationSettings();
                animationSettings.Type = SimultaneouslyDrawingCheckBox.IsChecked
                    ? MapAnimationType.DrawWithAnimation
                    : MapAnimationType.DrawAfterAnimation;
                animationSettings.Length = 2000;
                animationSettings.Easing = Easing.Linear;
                
                await MapView.ZoomToExtentAsync(new PointShape(currentLocation), MapView.MapScale, angle, animationSettings);

                _currentPointIndex++;
            });
        }

        private void UpdateVisitedRoutes(int currentPointIndex)
        {
            if (currentPointIndex == 0 || _previousVertex >= currentPointIndex) return;
            var lineShape = new LineShape();
            for (var i = _previousVertex; i <= currentPointIndex; i++)
            {
                lineShape.Vertices.Add(_gpsPoints[i]);
            }

            var lineFeature = new Feature(lineShape);
            _visitedRoutesLayer.InternalFeatures.Add(lineFeature);
            _previousVertex = currentPointIndex;
        }

        private static double GetRotationAngle(int currentIndex, IReadOnlyList<Vertex> gpsPoints)
        {
            Vertex currentLocation;
            Vertex nextLocation;

            if (currentIndex < gpsPoints.Count - 1)
            {
                currentLocation = gpsPoints[currentIndex];
                nextLocation = gpsPoints[currentIndex + 1];
            }
            else
            {
                currentLocation = gpsPoints[currentIndex - 1];
                nextLocation = gpsPoints[currentIndex];
            }

            double angle;
            if (nextLocation.X - currentLocation.X != 0)
            {
                var dx = (nextLocation.X - currentLocation.X);
                var dy = (nextLocation.Y - currentLocation.Y);

                angle = Math.Atan2(dx, dy) / Math.PI * 180; // get the angle in degrees from 
                angle = -angle;
            }
            else
            {
                angle = nextLocation.Y - currentLocation.Y >= 0 ? 0 : 180;
            }
            return angle;
        }
    }
}