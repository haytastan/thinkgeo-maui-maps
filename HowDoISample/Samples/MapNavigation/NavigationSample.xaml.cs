﻿using ThinkGeo.Core;

namespace ThinkGeo.UI.Maui.HowDoI;

public partial class NavigationSample
{
    private bool _initialized;
    public NavigationSample()
    {
        InitializeComponent();
    }

    private async void NavigationSample_OnSizeChanged(object sender, EventArgs e)
    {
        if (_initialized)
            return;
        _initialized = true;

        // Set the map's unit of measurement to meters(Spherical Mercator)
        MapView.MapUnit = GeographyUnit.Meter;

        // Add ThinkGeo Cloud Maps as the background 
        var backgroundOverlay = new ThinkGeoCloudRasterMapsOverlay("9ap16imkD_V7fsvDW9I8r8ULxgAB50BX_BnafMEBcKg~", "vtVao9zAcOj00UlGcK7U-efLANfeJKzlPuDB9nw7Bp4K4UxU_PdRDg~~", ThinkGeoCloudRasterMapsMapType.Light_V2_X2);
        backgroundOverlay.TileCache = new FileRasterTileCache(FileSystem.Current.CacheDirectory, "ThinkGeoCloudRasterMaps");
        MapView.Overlays.Add(backgroundOverlay);

        // create a point for empire state building, convert the Lat/Lon (srid:4326) to spherical mercator(srid:3857), which is the projection of the background
        var empireStateBuilding = ProjectionConverter.Convert(4326, 3857, new PointShape(-73.985665442769, 40.7484366107232));

        var marker = new Marker
        {
            PositionX = empireStateBuilding.X,
            PositionY = empireStateBuilding.Y,
            ImageSource = (StreamImageSource)ImageSource.FromResource("HowDoISample.Resources.Images.marker.png"),
            YOffset = -17
        };

        var simpleMarkerOverlay = new SimpleMarkerOverlay();
        MapView.Overlays.Add("simpleMarkerOverlay", simpleMarkerOverlay);
        simpleMarkerOverlay.Markers.Add(marker);

        MapView.IsRotationEnabled = true;
        
        // events
        DefaultExtentButton.Clicked += async (_, _) =>
            await MapView.ZoomToExtentAsync(empireStateBuilding, MapView.ZoomLevelSet.ZoomLevel14.Scale, -30);
        NorthUpButton.Clicked += async (_, _) => 
            await MapView.ZoomToExtentAsync(MapView.CenterPoint, MapView.MapScale, 0); 
        ThemeCheckBox.CheckedChanged += async (_, args) =>
        {
            backgroundOverlay.MapType = args.Value
                ? ThinkGeoCloudRasterMapsMapType.Dark_V2_X2
                : ThinkGeoCloudRasterMapsMapType.Light_V2_X2;
            await backgroundOverlay.RefreshAsync();
        };

        MapView.MapRotation = -30;
        MapView.MapScale = MapView.ZoomLevelSet.ZoomLevel14.Scale;
        MapView.CenterPoint = empireStateBuilding;
        await MapView.RefreshAsync();
    }
}