﻿using ThinkGeo.Core;

namespace ThinkGeo.UI.Maui.HowDoI;

[XamlCompilation(XamlCompilationOptions.Compile)]
public partial class ZoomToBlackHoleSample
{
    private List<(PointShape centerPoint, double scale)> _zoomingExtents;
    private int _currentPointIndex;

    private bool _initialized;
    public ZoomToBlackHoleSample()
    {
        InitializeComponent();
        ThinkGeoDebugger.LogLevel = ThinkGeoLogLevel.All;
    }
    private void ZoomToBlackHoleSample_OnSizeChanged(object sender, EventArgs e)
    {
        if (_initialized)
            return;
        _initialized = true;

        // The DefaultAnimationSettings affects the animation of double click
        MapView.DefaultAnimationSettings.Length = 2000;

        ZoomToBlackHoleButton.Clicked += async (_, _) =>
        {
            _currentPointIndex = 2;
            MapView.OverlayDrawn -= MapView_OverlayDrawn;
            MapView.OverlayDrawn += MapView_OverlayDrawn;
            await MapView.ZoomToExtentAsync(_zoomingExtents[1].centerPoint, _zoomingExtents[1].scale, 0);
        };

        DefaultExtentButton.Clicked += async (_, _) =>
        {
            MapView.OverlayDrawn -= MapView_OverlayDrawn;
            await MapView.ZoomToExtentAsync(_zoomingExtents[0].centerPoint, _zoomingExtents[0].scale, 0);
        };
        
        // stop the auto zooming whenever touching the map
        MapView.TouchDown += (_, _) => MapView.OverlayDrawn -= MapView_OverlayDrawn;

        ZoomMapTool.ZoomInButton.Clicked += (_, _) => MapView.OverlayDrawn -= MapView_OverlayDrawn;
        ZoomMapTool.ZoomOutButton.Clicked += (_, _) => MapView.OverlayDrawn -= MapView_OverlayDrawn;

        MapView.CurrentExtentChanged += MapViewOnCurrentExtentChanged;
        _zoomingExtents = GetZoomingExtents();
    }

    private void MapViewOnCurrentExtentChanged(object sender, CurrentExtentChangedMapViewEventArgs e)
    {
        if (!e.IsMapScaleChanged)
            return;

        foreach (var overlay in MapView.Overlays)
        {
            if (overlay is not LayerOverlay layerOverlay)
                continue;
            if (layerOverlay.Layers[0] is not GeoImageLayer geoImageLayer)
                continue;
            if (MapView.MapScale < geoImageLayer.LowerThreshold)
            {
                layerOverlay.Opacity = 0;
                continue;
            }
            if (MapView.MapScale > geoImageLayer.UpperThreshold)
            {
                layerOverlay.Opacity = 0;
                continue;
            }

            var upperRatio = 1 - (MapView.MapScale / geoImageLayer.UpperThreshold);
            var lowerRatio = (MapView.MapScale / geoImageLayer.LowerThreshold);

            if (upperRatio < 0.4)
                layerOverlay.Opacity = upperRatio * 2.5;
            else if (lowerRatio < 2)
                layerOverlay.Opacity = lowerRatio / 2;
            else
                layerOverlay.Opacity = 1;
        }
    }

    private async void MapView_OverlayDrawn(object sender, OverlaysDrawnMapViewEventArgs e)
    {
        if (_currentPointIndex >= _zoomingExtents.Count)
        {
            MapView.OverlayDrawn -= MapView_OverlayDrawn;
            return;
        }

        var currentLocation = _zoomingExtents[_currentPointIndex];
        _currentPointIndex++;

        var animationSettings = new AnimationSettings()
        { Length = 3000, Type = MapAnimationType.DrawWithAnimation };

        await MapView.ZoomToExtentAsync(currentLocation.centerPoint, currentLocation.scale, 0, animationSettings);
    }

    private List<(PointShape CenterPoint, double Scale)> GetZoomingExtents()
    {
        var zoomingExtents = new List<(PointShape CenterPoint, double Scale)>();

        var firstLayer = (GeoImageLayer)(((LayerOverlay)MapView.Overlays[0]).Layers[0]);
        zoomingExtents.Add((firstLayer.CenterPoint, firstLayer.Scale));

        for (int i = 1; i < MapView.Overlays.Count; i++)
        {
            var overlay = MapView.Overlays[i];
            if (overlay is not LayerOverlay layerOverlay)
                continue;
            if (layerOverlay.Layers.Count < 0)
                continue;
            var geoImageLayer = (GeoImageLayer)layerOverlay.Layers[0];
            if (geoImageLayer == null)
                continue;
            zoomingExtents.Add((geoImageLayer.CenterPoint, geoImageLayer.UpperThreshold));
        }

        var lastLayer = (GeoImageLayer)(((LayerOverlay)MapView.Overlays[^1]).Layers[0]);
        zoomingExtents.Add((lastLayer.CenterPoint, lastLayer.Scale));
        return zoomingExtents;
    }
}