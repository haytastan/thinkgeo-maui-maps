﻿namespace ThinkGeo.UI.Maui.HowDoI
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DescriptionView : ContentView
    {
        public static readonly BindableProperty DescriptionProperty = BindableProperty.Create(nameof(Description), typeof(string), typeof(DescriptionView), default(string));

        public string Description
        {
            get => (string)GetValue(DescriptionProperty);
            set => SetValue(DescriptionProperty, value);
        }

        public DescriptionView()
        {
            InitializeComponent();
            this.BindingContext = this;
        }
    }
}