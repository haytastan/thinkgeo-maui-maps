﻿namespace ThinkGeo.UI.Maui.HowDoI
{
    /// <summary>
    ///     Learn how to programmatically zoom, pan, and rotate the map control.
    /// </summary>
    public partial class SamplePage : ContentPage
    {
        public SamplePage()
        {
            InitializeComponent();
        }

        public static readonly BindableProperty DescriptionProperty = BindableProperty.Create(
            propertyName: nameof(Description),
            returnType: typeof(string),
            declaringType: typeof(SamplePage),
            defaultValue: null);

        public string Description
        {
            get => (string)GetValue(DescriptionProperty);
            set => SetValue(DescriptionProperty, value);
        }
    }
}