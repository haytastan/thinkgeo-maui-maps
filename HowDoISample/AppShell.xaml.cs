﻿using HowDoISample.Models;
using ThinkGeo.UI.Maui.HowDoI;

namespace HowDoISample
{
    public partial class AppShell : Shell
    {
        public static readonly BindableProperty CurrentPageTitleProperty =
            BindableProperty.Create(nameof(CurrentPageTitle), typeof(string), typeof(AppShell), string.Empty);

        public string CurrentPageTitle
        {
            get => (string)GetValue(CurrentPageTitleProperty);
            set => SetValue(CurrentPageTitleProperty, value);
        }

        public AppShell()
        {
            InitializeComponent();
            BindingContext = this; // Ensure the Shell's binding context is set to its code-behind.
        }

        public async Task NavigateFromMenu(SampleMenuItem sample)
        {
            // Set the title and description
            if (Current is AppShell shell)
            {
                shell.CurrentPageTitle = sample.Title;
            }

            // Add the route of your page (e.g., "YourPageRoute") to navigate to that page
            await Current.GoToAsync($"{sample.Id}");
            var currentPage = Current.CurrentPage;

            if (currentPage is SamplePage samplePage)
            {
                samplePage.Description = sample.Description;
                samplePage.Title = sample.Title;
            }

            FlyoutIsPresented = false;
        }
    }
}