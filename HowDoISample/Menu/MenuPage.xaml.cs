﻿using HowDoISample.Menu.ViewModels;
using HowDoISample.Models;
using System.ComponentModel;
using ThinkGeo.Core;

namespace HowDoISample
{
    [DesignTimeVisible(false)]
    public partial class MenuPage : ContentPage
    {
        private readonly MenuViewModel sampleMenu;

        public MenuPage()
        {
            InitializeComponent();
            sampleMenu = BindingContext as MenuViewModel;
            // Set the first item as the selected item
            ListViewMenu.SelectedItem = sampleMenu?.SampleMenuItems[0][0];
        }

        private async void ListViewMenu_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var selectedMenuItem = e.SelectedItem as SampleMenuItem;
            if (selectedMenuItem == null) return;

            // Navigate to the selected page
            var targetType = Type.GetType(selectedMenuItem.Id);
            if (targetType == null) return;

            if (Application.Current?.MainPage is not AppShell shell) return;
            await shell.NavigateFromMenu(selectedMenuItem);
            Shell.Current.FlyoutIsPresented = false; 
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            var stackLayout = (StackLayout)sender;
            var item = (TapGestureRecognizer)stackLayout.GestureRecognizers[0];
            var id = item.CommandParameter;
            var i = sampleMenu.SampleMenuItems.IndexOf((MenuGroup)id);
            sampleMenu.ToggleGroupExpanded(i);
            ListViewMenu.ItemsSource = sampleMenu.SampleMenuItems;
        }
    }

    public static class RouteRegistrar
    {
        private static Dictionary<string, Type> _routes = new Dictionary<string, Type>();

        public static void RegisterRoute(string route, Type pageType)
        {
            Routing.RegisterRoute(route, pageType);
            _routes[route] = pageType;
        }

        public static Type GetPageType(string route)
        {
            if (_routes.ContainsKey(route))
                return _routes[route];

            return null;
        }
    }
}